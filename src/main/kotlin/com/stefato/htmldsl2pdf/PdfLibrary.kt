package com.stefato.htmldsl2pdf

enum class PdfLibrary (val className:String){
    NONE(""),
    WkHtmlToPdf("com.github.jhonnymertz.wkhtmltopdf.wrapper.Pdf"),
    OpenHtmlToPdf("com.openhtmltopdf.pdfboxout.PdfRendererBuilder"),
    IText7("com.itextpdf.html2pdf.HtmlConverter")
}