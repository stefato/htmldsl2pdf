package com.stefato.htmldsl2pdf

import com.stefato.htmldsl2pdf.creators.PdfCreator
import kotlinx.html.HTML
import kotlinx.html.html
import kotlinx.html.stream.createHTML
import java.io.File
import java.io.FileOutputStream

class HtmlPdf(config: HtmlPdfConfig) {
    var config: HtmlPdfConfig = config
        set(value) {
            creator = config.libSpecificCreator
            field = value
        }

    private var creator: PdfCreator = config.libSpecificCreator

    fun savePdfFromHtml(path: String, block: HTML.() -> Unit = {}): File? = File(path).apply {
        val byteArray = createPdf(buildHtmlString(block))
        FileOutputStream(this).use {
            it.write(byteArray)
        }
    }

    fun createPdfFromHtml(block: HTML.() -> Unit = {}): ByteArray? = createPdf(buildHtmlString(block))

    private fun createPdf(html: String): ByteArray? {
        val byteArray = setContent(html).byteArray()
        creator = config.libSpecificCreator
        return byteArray
    }

    private fun setContent(html: String) = creator.apply { setContent(html) }

    private fun buildHtmlString(block: HTML.() -> Unit = {}) = createHTML(prettyPrint = false, xhtmlCompatible = true)
            .html { block() }

}