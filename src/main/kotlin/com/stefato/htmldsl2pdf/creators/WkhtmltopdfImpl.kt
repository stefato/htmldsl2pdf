package com.stefato.htmldsl2pdf.creators

import com.github.jhonnymertz.wkhtmltopdf.wrapper.Pdf
import java.io.File

class WkhtmltopdfImpl : PdfCreator {

    private val pdfInstance = Pdf()

    override fun setContent(string: String) {
        pdfInstance.addPageFromString(string)
    }
    override fun byteArray(): ByteArray? = pdfInstance.pdf
}