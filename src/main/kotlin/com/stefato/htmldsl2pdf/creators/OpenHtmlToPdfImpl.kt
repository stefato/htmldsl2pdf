package com.stefato.htmldsl2pdf.creators

import com.openhtmltopdf.pdfboxout.PdfRendererBuilder
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.FileOutputStream

class OpenHtmlToPdfImpl : PdfCreator {
    private val builder = PdfRendererBuilder()

    override fun byteArray(): ByteArray? {
        val os  = ByteArrayOutputStream()
        builder.toStream(os)
        builder.run()
        return os.toByteArray()
    }

    override fun setContent(string: String) {
        builder.withHtmlContent(string, File("/").toURI().toString())
    }
}