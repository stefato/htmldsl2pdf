package com.stefato.htmldsl2pdf.creators

import com.itextpdf.html2pdf.HtmlConverter
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.FileOutputStream

class IText7Impl : PdfCreator {
    private var htmlString: String = ""
    override fun setContent(string: String) {
        htmlString = string
    }

    override fun byteArray(): ByteArray? =
            ByteArrayOutputStream().apply {
                HtmlConverter.convertToPdf(htmlString, this)
            }.toByteArray()

}