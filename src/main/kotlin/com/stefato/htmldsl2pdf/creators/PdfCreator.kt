package com.stefato.htmldsl2pdf.creators

interface PdfCreator {
    fun byteArray(): ByteArray?
    fun setContent(string: String)
}