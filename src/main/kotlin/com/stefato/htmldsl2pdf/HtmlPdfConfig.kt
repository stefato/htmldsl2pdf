package com.stefato.htmldsl2pdf

import com.stefato.htmldsl2pdf.creators.IText7Impl
import com.stefato.htmldsl2pdf.creators.OpenHtmlToPdfImpl
import com.stefato.htmldsl2pdf.creators.PdfCreator
import com.stefato.htmldsl2pdf.creators.WkhtmltopdfImpl

class HtmlPdfConfig(
        private val library: PdfLibrary
) {
    val libSpecificCreator: PdfCreator by lazy { chooseLib() }

    init {
        if (!isPresent(library.className)) throw ClassNotFoundException("${library.className} not found")
    }

    private fun isPresent(className: String): Boolean {
        try {
            Class.forName(className)
        } catch (e: ClassNotFoundException) {
            return false
        }
        return true
    }

    private fun chooseLib() = when (library) {
        PdfLibrary.WkHtmlToPdf -> WkhtmltopdfImpl()
        PdfLibrary.OpenHtmlToPdf -> OpenHtmlToPdfImpl()
        PdfLibrary.IText7 -> IText7Impl()
        PdfLibrary.NONE -> throw IllegalStateException("HtmlPdf ist not initialized. Please provide a HtmlPdfConfig")
    }
}