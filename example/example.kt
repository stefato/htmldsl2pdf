import com.stefato.htmldsl2pdf.HtmlPdf
import com.stefato.htmldsl2pdf.HtmlPdfConfig
import com.stefato.htmldsl2pdf.PdfLibrary
import kotlinx.css.*
import kotlinx.css.Float
import kotlinx.html.*
import java.io.File
import java.io.FileOutputStream

const val basepath = "src/output"
const val fileExt = ".pdf"

fun main(args: Array<String>) {
    val config = HtmlPdfConfig(library = PdfLibrary.WkHtmlToPdf)
    val htmlPdf = HtmlPdf(config)

    val byteArray = htmlPdf.createPdfFromHtml {
        head {
            style { unsafe { +cssString } }
        }
        body {
            div(classes = "header") {
                img { src = File("src/main/resources/exampleLogo.png").absoluteFile.toURI().toString() }
            }
            createTable()
        }
    }

    FileOutputStream("$basepath/generated$fileExt").use {
        it.write(byteArray)
    }

}

fun BODY.createTable() = table {
    caption {
        attributes["style"] = "font-size: 30px"
        +"HTML Table"
    }
    thead {
        tr {
            tableHeadings.map { th { +it } }
        }
    }
    tbody {
        for (i in 0..20) {
            for (data in tableData) {
                tr {
                    data.map { td { +it } }
                }
            }
        }
    }
}

val tableHeadings = listOf("Company", "Contact", "Country", "Revenue")

val tableData = listOf(
        listOf("Alfreds Futterkiste", "Maria Anders", "Germany", "293.329.443"),
        listOf("Centro comercial Moctezuma", "Francisco Chang", "Mexico", "121.437.345"),
        listOf("Ernst Handel", "Roland Mendel", "Austria", "594.230.643"),
        listOf("Island Trading", "Helen Bennett", "UK", "134.632.864"),
        listOf("Laughing Bacchus Winecellars", "Yoshi Tannamuri", "Canada", "922.231.123"),
        listOf("Magazzini Alimentari Riuniti", "Giovanni Rovelli", "Italy", "948.123.594")
)

val cssString = CSSBuilder().apply {
    table {
        fontFamily = "arial, sans-serif"
        borderCollapse = BorderCollapse.collapse
        width = LinearDimension("100%")
    }

    rules.add(Rule(".header img") {
        float = Float.left
        width = LinearDimension("200px")
        height = LinearDimension("200px")
    })

    rules.add(Rule("thead") {
        backgroundColor = Color("#508ABB")
    })

    rules.add(Rule("td,th") {
        border = "1px solid #dddddd"
        textAlign = TextAlign.left
        padding = "8px"
    })

    rules.add(Rule("tbody > tr:nth-child(odd)") {
        backgroundColor = Color("#f4fbff")
    })

}.toString()
